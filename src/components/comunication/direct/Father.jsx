import React from "react";
import Son from "./Son";

export default props => {
    return (
        <div>
            <Son text="First Son" number="1" boll={true}></Son>
            <Son text="Second Son" number="2" boll={false}></Son>
        </div>
    )
}