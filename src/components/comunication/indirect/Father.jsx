import React, { useState } from "react";
import Son from './Son'

export default props => {
    const [name, setName] = useState('?');
    const [age, setAge] = useState(0);
    const [access, setAccess] = useState(false);

    function showInfo(name, age, access) {
        setName(name);
        setAge(age);
        setAccess(access);
    }

    return (
        <div>
            <div>
                <span>{name ? name : '?'} </span>
                <span><strong>{age} </strong></span>
                <span>{access ? 'Allowed' : 'Denied'}</span>
            </div>
            <Son whenClick={showInfo} />
        </div>
    )
}