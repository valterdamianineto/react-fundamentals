import React from "react"

export default () => {
    const msg = 'Verifying access data'
    return (
        <div>
            <h1>Restricted Access System</h1>
            <h2>{msg}</h2>
        </div>
    )
}