import React from "react";

export default props => {
    return (
        <div>
            <strong>{props.groupName}</strong>
            <span> - {props.requiredAccess}</span>
        </div>
    )
}