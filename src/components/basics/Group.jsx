import React, { cloneElement } from "react";
// import React, { Children, cloneElement } from "react";
// import GroupMember from "./GroupMember"
// import faker from 'faker'

export default props => {
    return (
        <div>
            {props.children.map((groupMember, i) => {
                return cloneElement(groupMember, { ...props, key: i })
            })
            }

            {/*
                Children.map(props.children, groupMember => {
                    return cloneElement(groupMember, props)
                })
            */}

            {/* 
                {props.children} 
            */}

            {/* 
                <GroupMember groupName={faker.name.jobArea()}/>
                <GroupMember groupName={faker.name.jobArea()}/>
                <GroupMember groupName={faker.name.jobArea()}/> 
            */}
        </div>
    )
}