import React from "react";

export default (props) => {
    const min = 1;
    const max = 5;
    const levelRequired = parseInt(Math.random() * (max - min)) + min;
    const level = props.userLevel >= levelRequired ? 'Authorized' : 'Unauthorized'
    const AccessButton = level === 'Authorized' ? 'Acess' : 'Try Again'
    return (
        <>
            <strong> {level}</strong>
            <br />
            <br />
            <button>{AccessButton}</button>
        </>
    )
}