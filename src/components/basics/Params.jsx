import React from "react"
import AcessLevel from "./AcessLevel"

export default (props) => {
    const status = props.UserId === '848fsd384ds83d38dsd' ? 'ok' : 'denied'

    return (
        <div>
            <h3>{props.title}</h3>
            <p>
                The user
                <strong> {props.name}</strong> with id
                <strong> {props.UserId}</strong> has an id with status
                <strong> {status}</strong> and had your access
                <AcessLevel 
                    userLevel={props.permiLevel}
                />
            </p>
        </div>
    )
}