import React from "react";
// import faker from "faker"
import users from '../../data/users'

export default () => {
    const lines = users.map(user => {
        return (
            <li key={user.id}>{user.id} # {user.name} -- {user.permissionAccess}</li>
        )
    })
    return (
        <div>
            <ul style={{ listStyle: "none" }}>
                {lines}
            </ul>
            {/* <ul>
                <li>{faker.name.firstName()} {faker.name.lastName()} - {faker.datatype.number(5)}</li>
                <li>{faker.name.firstName()} {faker.name.lastName()} - {faker.datatype.number(5)}</li>
                <li>{faker.name.firstName()} {faker.name.lastName()} - {faker.datatype.number(5)}</li>
                <li>{faker.name.firstName()} {faker.name.lastName()} - {faker.datatype.number(5)}</li>
                <li>{faker.name.firstName()} {faker.name.lastName()} - {faker.datatype.number(5)}</li>
            </ul> */}
        </div>
    )
}