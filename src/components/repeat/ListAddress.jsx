import './ListAddress.css'
import React from "react";
import userData from '../../data/userData'

export default () => {
    const userInfo = userData.map((address, i) => {
        const addressStyle = {
            fontWeight: 100,
        }
        return (
            <tr key={address.id} className={i % 2 == 0 ? 'yes' : 'no'}>
                <td>{address.name}</td>
                <td style={addressStyle}> {address.street}, {address.city} - {address.state}</td>
            </tr>
        )
    })
    return (
        <div className="UserAdress">
            <table>
                <tr>
                    <th className="user-name">Name</th>
                    <th className="user-address">Address</th>
                </tr>
                <tbody>
                    {userInfo}
                </tbody>
            </table>
        </div>
    )
}