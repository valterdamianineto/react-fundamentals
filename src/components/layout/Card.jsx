import "./Card.css";
import React from "react";

export default (props) => {
    const styleCard = {
        backgroundColor: props.bgColor || '#04395E',
        borderColor: props.bdColor || '#031D44'
    }
    const styleTitle = {
        color: props.color || '#ffffff'

    }
    return (
        <div className="Card" style={styleCard}>
            <label className="Title" style={styleTitle}>{props.title}</label>
            <div className="Content">{props.children}</div>
        </div>
    )
}