import './Input.css'
import React, { useState } from 'react'

export default props => {
    const [inputValue, setValue] = useState('Initial')

    function whenChange(e) {
        setValue(e.target.value)
    }
    return (
        <div className="Input">
            <h2>{inputValue}</h2>
            <div>
                <span>ReadOnly Component</span>
                <input value={inputValue} readOnly />
            </div>
            <div>
                <span>Controled Component</span>
                <input value={inputValue} onChange={whenChange} />
            </div>
            <div>
                <span>Uncontroled Component</span>
                <input value={undefined} onChange={whenChange} />
            </div>
        </div>
    )
}