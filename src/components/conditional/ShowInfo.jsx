import React from "react";

export default props => {
    const isPar = props.number % 2 === 0;
    console.log(isPar);
    return (
        <div>
            {isPar ? <span>Par</span> : <span>Impar</span>}
        </div>
    )
}