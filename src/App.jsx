import './App.css'
import React from 'react'
import faker from 'faker'
import First from './components/basics/First'
import Params from './components/basics/Params'
import Card from './components/layout/Card'
import Group from './components/basics/Group'
import GroupMember from './components/basics/GroupMember'
import ListUsers from './components/repeat/ListUsers'
import ListAddress from './components/repeat/ListAddress'
import ShowInfo from './components/conditional/ShowInfo'
import DirectFather from './components/comunication/direct/Father'
import IndirectFather from './components/comunication/indirect/Father'
import Input from './components/form/Input'

export default () => (
    <div className="App">
        <div className="Cards">
            <Card title="Controled Comunication">
                <Input />
            </Card>
            <Card title="Indirect Comunication">
                <IndirectFather />
            </Card>
            <Card title="Direct Comunication">
                <DirectFather />
            </Card>
            <Card title="Conditional">
                <ShowInfo number={faker.datatype.number()} />
            </Card>
            <Card title="Address List">
                <ListAddress />
            </Card>
            <Card title="User List">
                <ListUsers />
            </Card>
            <Card
                title="Available Groups"
                bgColor="#969696"
                bdColor="#969696"
            >
                <Group requiredAccess={2}>
                    <GroupMember groupName={faker.name.jobArea()} />
                    <GroupMember groupName={faker.name.jobArea()} />
                    <GroupMember groupName={faker.name.jobArea()} />
                </Group>
            </Card>
            <Card title="Login">
                <Params
                    title="Data Checked"
                    name="Valter D Neto"
                    UserId="848fsd384ds83d38dsd"
                    permiLevel={2}
                />
            </Card>
            <Card
                title="Acess Verification"
                bgColor="#eed202"
                bdColor="black"
                color="black"
            >
                <First />
            </Card>
        </div>
    </div>
)